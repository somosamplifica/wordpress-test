# Full Stack Developer / WordPress 

## Objetivo
Crear una página 404 donde se vea la siguiente información:

- **Módulo 1: “Ayudanos a encontrarlos”** Mostrar las últimas búsquedas activas de Missing Children Argentina con la API pública de [No encontrado](https://noencontrado.org/404/). Los datos a mostrar son Foto, Nombre y Fecha de desaparición y Localidad.

- **Módulo 2: "Dataset"** Mostrar los últimos 4 Custom Post Type “Dataset” de la categoría “Administración” donde muestre el título, excerpt, boton con link, un custom field tipo Fecha.

## Estructura
- Instalar WordPress con el boilerplate Bedrock y el starter theme Sage. Esto nos permitirá la utilización de composer, webpack, controladores, entre otras ventajas dentro de la aplicación.
- Sage, utiliza [Sober](https://github.com/soberwp/controller#template-override-option) para utilizar controladores y en la documentación explica cómo crear una página de 404 con su correspondiente controlador. 
- Instalar [ACF Local JSON](https://github.com/MWDelaney/sage-advanced-custom-fields) para que los ACF creados queden guardados en jSON y puedan ser sincronizados.
- La llamada a la API y la lógica debe estar dentro del Controlador y la vista debe recibir solamente la información a mostrar.
- El Frontend debe estar desarrollado con Bootstrap y cada una de las búsquedas/entradas debe ser una card.
- Utilizar alguna de los [ejemplos de Bootstrap](https://getbootstrap.com/docs/5.1/examples/) para crear el header, body y footer del sitio.
## Recursos
- [API No Encontrado](https://github.com/armageAR/API.NoEncontrado.ORG) 
- [Sage](https://github.com/roots/sage)
- [Bedrock](https://github.com/roots/bedrock)


## Deadline
Enviar Pull Request con el código compilado a este repositorio.
 
## Tips
- Usar la última versión estable posible: PHP, WordPress core, plugins, packages
- Utilizar las mejores prácticas en toda la aplicación.
- DRY: Don't Repeat Yourself (¡No Te Repitas!) ten en cuenta este principio.
- Tener en cuenta la seguridad de la aplicación.    
- Sé proactivo. Si crees que algo de esta prueba se puede mejorar aceptamos sugerencias.
- Estate preparado. Vamos a necesitar que nos expliques el código.